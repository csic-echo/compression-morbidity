################################################
# Decomposition of ChrDFLE by sex and education
################################################

library(data.table)
library(dplyr)
library(xtable)
source("scripts/utils.R")

gender_ed <- readRDS("output/data/gender_ed.rds")
deaths_ed <- readRDS("output/data/deaths_education.rds")
life_tables_edu_y <- readRDS("output/data/life_tables_edu_y.rds")
chr_free_five_tables_edu_y <- readRDS("output/data/chr_free_five_tables_edu_y.rds")
gender_year <- readRDS("output/data/gender_year.rds")
life_tables_edu_y_low_high <- readRDS("output/data/life_tables_edu_y_low_high.rds")
life_tables_edu_y_primary_high <- readRDS("output/data/life_tables_edu_y_primary_high.rds")

AgeN <- c(65,70,75,80,85,90)

decomp_chrdfle_tab_edu = list()
for (i in seq_along(gender_ed$education)) {
  selection_row = deaths_ed$sex[1:6] == gender_ed[i, sex] &
    deaths_ed$education[c(1,2,13,14,25,26)] == gender_ed[i, education]
  X <- AdjustedArriaga_dfle(life_tables_edu_y[[i]]$Lx,life_tables_edu_y[[i+6]]$Lx,
                            chr_free_five_tables_edu_y[[i]]$disrate,chr_free_five_tables_edu_y[[i+6]]$disrate, Age = AgeN)

  decomp = as.data.frame(X)
  decomp$sex = gender_ed[i, sex]
  decomp$education = gender_ed[i, education]
  decomp_chrdfle_tab_edu[[gender_ed[i, output_name]]] = decomp
}

# Estimate chrfle changes according to mortality and disability
# ------------------------------------------------------------
chrdfle_decomp_tab_edu = list()
for (i in seq_along(gender_ed$education)) {
  selection_row = deaths_ed$sex[1:6] == gender_ed[i, sex] &
    deaths_ed$education[c(1,2,13,14,25,26)] == gender_ed[i, education]
  
  lx_a <- life_tables_edu_y[[i]][,7]
  lx_b <- life_tables_edu_y[[i+6]][,7]
  ex <- life_tables_edu_y[[i]][,11]
  ex_2 <- life_tables_edu_y[[i+6]][,11]
  Lx_dis_a <- chr_free_five_tables_edu_y[[i]][,4]
  Lx_dis_b <- chr_free_five_tables_edu_y[[i+6]][,4]
  MOR <- decomp_chrdfle_tab_edu[[i]][,2]
  DIS <- decomp_chrdfle_tab_edu[[i]][,3]  
  
  chrdfle_decomp = as.data.frame(dfle_fun_decomp(Lx_dis_a = Lx_dis_a, Lx_dis_b = Lx_dis_b, lx_a = lx_a, lx_b=lx_b, MOR = MOR, DIS = DIS,
                                                 ex = ex, ex_2=ex_2, age=AgeN))
  
  chrdfle_decomp <- chrdfle_decomp %>% mutate(diff_general = dfle_2-dfle_1) %>% 
    # difference due to mortality
    mutate(diff_mor = dfle_mor - dfle_1) %>% 
    # difference due to disability
    mutate(diff_dis = dfle_dis - dfle_1) %>%
    # pure mortality differences
    mutate(diff_ex = ex_2-ex)
  
  chrdfle_decomp$sex = gender_ed[i, sex]
  chrdfle_decomp$education = gender_ed[i, education]
  chrdfle_decomp_tab_edu[[gender_ed[i, output_name]]] = chrdfle_decomp
}  

for (i in seq_along(chrdfle_decomp_tab_edu)) {
  select_columns = c("age", "sex", "education", "diff_ex", "diff_general", "diff_mor", "diff_dis")
  print(
    xtable(chrdfle_decomp_tab_edu[[i]][, select_columns], 
           caption = paste0("DLFE_Decomp - ", names(chrdfle_decomp_tab_edu[i]))
    ),
    caption.placement = "top",
    file = paste0("reports/echo-hle-report/tables/chrdfle_decomp_table_", 
                  paste0(strsplit(tolower(names(chrdfle_decomp_tab_edu[i])), " ")[[1]], collapse  = "_"), 
                  ".tex")
  )
}

# pool to have one table
pooled_chrfle_decomp_tables_edu = rbindlist(chrdfle_decomp_tab_edu)
pooled_chrfle_decomp_tables_edu$education = factor(pooled_chrfle_decomp_tables_edu$education,
                                                 labels = c("low education", "primary education", "high education"),
                                                 levels = 1:3
)
pooled_chrfle_decomp_tables_edu$sex = factor(pooled_chrfle_decomp_tables_edu$sex,
                                           labels = c("female", "male"),
                                           levels = 1:2
)

## Decompose the gap between educational groups between the two periods - ChrDFLE
## ------------------------------------------------------------------------------

# low vs. high
chr_free_five_tables_edu_y_low_high <- chr_free_five_tables_edu_y[c(1,2,7,8,5,6,11,12)]

decomp_tab_low_high = list()
for (i in seq_along(gender_year$sex)) {
  selection_row = deaths_ed$sex[1:4] == gender_year[i, sex] &
    deaths_ed$year[c(1,2,50,51)] == gender_year[i, year]
  
  X <- AdjustedArriaga_dfle(life_tables_edu_y_low_high[[i]]$Lx,life_tables_edu_y_low_high[[i+4]]$Lx,
                            chr_free_five_tables_edu_y_low_high[[i]]$disrate,chr_free_five_tables_edu_y_low_high[[i+4]]$disrate, Age = AgeN)
  decomp = as.data.frame(X)
  decomp$sex = gender_year[i, sex]
  decomp$year = gender_year[i, year]
  decomp_tab_low_high[[gender_year[i, output_name]]] = decomp
}

chr_free_decomp_low_high = list()
for (i in seq_along(gender_year$sex)) {
  selection_row = deaths_ed$sex[1:4] == gender_year[i, sex] &
    deaths_ed$year[c(1,2,50,51)] == gender_year[i, year]
  
  lx_a <- life_tables_edu_y_low_high[[i]][,7]
  lx_b <- life_tables_edu_y_low_high[[i+4]][,7]
  ex <- life_tables_edu_y_low_high[[i]][,11]
  ex_2 <- life_tables_edu_y_low_high[[i+4]][,11]
  Lx_dis_a <- chr_free_five_tables_edu_y_low_high[[i]][,4]
  Lx_dis_b <- chr_free_five_tables_edu_y_low_high[[i+4]][,4]
  MOR <- decomp_tab_low_high[[i]][,2]
  DIS <- decomp_tab_low_high[[i]][,3]  
  
  chr_free_decomp = as.data.frame(dfle_fun_decomp(Lx_dis_a = Lx_dis_a, Lx_dis_b = Lx_dis_b, lx_a = lx_a, lx_b=lx_b, MOR = MOR, DIS = DIS,
                                                  ex = ex, ex_2=ex_2, age=AgeN))
  
  chr_free_decomp <- chr_free_decomp %>% mutate(diff_general = dfle_2-dfle_1) %>% 
    # difference due to mortality
    mutate(diff_mor = dfle_mor - dfle_1) %>% 
    # difference due to disability
    mutate(diff_dis = dfle_dis - dfle_1) %>%
    # pure mortality differences
    mutate(diff_ex = ex_2-ex)
  
  chr_free_decomp$sex = gender_year[i, sex]
  chr_free_decomp$year = gender_year[i, year]
  chr_free_decomp_low_high[[gender_ed[i, output_name]]] = chr_free_decomp
}

# primary vs. high

# adjusting the lists for easier handling
chr_free_five_tables_edu_y_primary_high <- chr_free_five_tables_edu_y[c(3,4,9,10,5,6,11,12)]

decomp_tab_primary_high = list()
for (i in seq_along(gender_year$sex)) {
  selection_row = deaths_ed$sex[1:4] == gender_year[i, sex] &
    deaths_ed$year[c(1,2,50,51)] == gender_year[i, year]
  
  X <- AdjustedArriaga_dfle(life_tables_edu_y_primary_high[[i]]$Lx,life_tables_edu_y_primary_high[[i+4]]$Lx,
                            chr_free_five_tables_edu_y_primary_high[[i]]$disrate,chr_free_five_tables_edu_y_primary_high[[i+4]]$disrate, Age = AgeN)

  decomp = as.data.frame(X)
  decomp$sex = gender_year[i, sex]
  decomp$year = gender_year[i, year]
  decomp_tab_primary_high[[gender_year[i, output_name]]] = decomp
}

chr_free_decomp_primary_high = list()
for (i in seq_along(gender_year$sex)) {
  selection_row = deaths_ed$sex[1:4] == gender_year[i, sex] &
    deaths_ed$year[c(1,2,50,51)] == gender_year[i, year]
  
  lx_a <- life_tables_edu_y_primary_high[[i]][,7]
  lx_b <- life_tables_edu_y_primary_high[[i+4]][,7]
  ex <- life_tables_edu_y_primary_high[[i]][,11]
  ex_2 <- life_tables_edu_y_primary_high[[i+4]][,11]
  Lx_dis_a <- chr_free_five_tables_edu_y_primary_high[[i]][,4]
  Lx_dis_b <- chr_free_five_tables_edu_y_primary_high[[i+4]][,4]
  MOR <- decomp_tab_primary_high[[i]][,2]
  DIS <- decomp_tab_primary_high[[i]][,3]  
  
  chr_free_decomp = as.data.frame(dfle_fun_decomp(Lx_dis_a = Lx_dis_a, Lx_dis_b = Lx_dis_b, lx_a = lx_a, lx_b=lx_b, MOR = MOR, DIS = DIS,
                                                  ex = ex, ex_2=ex_2, age=AgeN))
  
  chr_free_decomp <- chr_free_decomp %>% mutate(diff_general = dfle_2-dfle_1) %>% 
    # difference due to mortality
    mutate(diff_mor = dfle_mor - dfle_1) %>% 
    # difference due to disability
    mutate(diff_dis = dfle_dis - dfle_1) %>%
    # pure mortality differences
    mutate(diff_ex = ex_2-ex)
  
  chr_free_decomp$sex = gender_year[i, sex]
  chr_free_decomp$year = gender_year[i, year]
  chr_free_decomp_primary_high[[gender_ed[i, output_name]]] = chr_free_decomp
}
# combine into one table
pooled_chrfle_decomp_primary_high = rbindlist(chr_free_decomp_primary_high)
