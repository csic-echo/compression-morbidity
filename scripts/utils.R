##########################
# auxiliary functions
##########################


# overwrite table and cor function to include missing data
table = function (...) base::table(..., useNA = 'ifany')
cor = function (...) stats::cor(..., use = "complete.obs")

getMax = function(x) {
  x = na.omit(x)
  if (length(x) == 0) {
    return(NA_real_)
  } else {
    return(max(x))
  }
}



# save pdfs
savepdf = function(file, width = 16, height = 10) {
    fname = paste0(file, ".pdf")
    pdf(fname, width = width / 2.54, height = height / 2.54,
        pointsize = 10)
    par(mgp=c(2.2, 0.45, 0), tcl=-0.4, mar = c(3.3, 3.6, 1.1, 1.1))

}

lookvar  = function(dat, varnames) {
    n  = names(dat)
    nn  = list()
        for (i in 1:length(varnames)) {
            nn[[i]]  = grep(varnames[i],n)
        }

    nn  = unlist(nn)

    if ( length(nn) >0 )
        {
         r  = n[nn]
         return(r)
        }
    else
    { return("No variables found")}
}

# abridged 5 year life table
# NEW: We are going to use our own function now (based on Preston, Heuveline, Guillot 2001)

LifeTableFiveYear <- function(Dx, N, Age){

    n <- length(Age)
    mx <- qx <- px <- dx <- lx <- Lx <- Tx <- ex <- rep(0,n)

    nx <- diff(Age)

    # calculate the life table death rate from observed deaths and exposure
    for(i in 1:n){
        # Dx[i] <- sum(rbinom(trials[i],1,qx[i]))
        mx[i] <- Dx[i]/N[i]
    }
    # assing the average life years lived per age interval
    for(i in 1:(n-1)){
        ax <- rep(2.5,n)
    }
    ax[n] <- 1/mx[n]

    # calculate the probability of dying within each age group (last forced to 1)
    for(i in 1:n){
        qx[i] <- (nx[i]*mx[i])/( 1 +(nx[i]-ax[i])*mx[i]) # Dx[i]/trials[i]
        qx <- replace(qx, qx[]==Inf, 0.0000001)
        qx[n] <- 1
        px[i] <- 1-qx[i]
    }
    # Number of survivors for each age group (lx, radix=100000)
    for(i in 1:(n-1)){
        lx[1] <- 1 #100000
        lx[i+1] <- lx[i] * (1-qx[i])
    }
    # Life table deaths as difference between survivors from the above age group and group x
    for(i in 1:(n-1)){
        dx[i] <- lx[i]-lx[i+1]
        # Life years lived in an age interval
        Lx[i] <- nx[i] * lx[i] - (nx[i] - ax[i]) * dx[i]
        # lx[1+i]*5 + ax[i]*dx[i]
        dx <- replace(dx,dx[]==0,0.00000001)
        Lx <- replace(Lx,Lx[]==0,0.00000001)
        lx <- replace(lx,lx[]==0,0.00000001)
    }
    # Last age interval
    Lx[n] <- lx[n]/mx[n]
    dx[n] <- lx[n]

    for(i in 1:n){
        Tx[i] <- sum(Lx[i:n])
        Tx <- replace(Tx,Tx[]==0,0.00000001)
        ex[i] <- Tx[i] / lx[i]
        ex[ex[] == NaN] <- 0
    }
    Tx[n] <- Lx[n]
    ex[n] <- Tx[n]/lx[n]

    return(lt = list(age = Age, Dx = Dx, ax = ax, mx = mx,
                     qx = qx, px = px, lx = lx,
                     dx = dx, Lx = Lx, Tx = Tx, ex = ex))
}


# life table single ages

lifeTableSingle <- function (Mx, age = NULL, radix = 1e+05) {

    if (is.null(age)) { age = c(0, 1, cumsum(rep(1, length(Mx) - 2))) }

    N <- length(Mx)
    w <- c(diff(age), 1)
    ax <- c(rep(.5, N - 1), 1 / Mx[N])
    Nax <- w * ax
    qx <- (w * Mx)/(1 + (w - ax) * Mx)
    qx[N] <- 1
    px <- 1 - qx

    lx <- c(radix, radix * (cumprod(px[-N])))
    dx <- -diff(c(lx, 0))
    #Lx[i] <- w * lx - (w - ax) * dx
    Lx <- lx[-1] * w[-N] + dx[-N] * Nax[-N]
    Lx[N] <- lx[N]/mx[N]
    Tx <- rev(cumsum(rev(Lx)))
    ex <- Tx/lx

    return(lt = list(
        age = age, ax = ax, mx = Mx,
        qx = qx, px = px, lx = lx,
        dx = dx, Lx = Lx, Tx = Tx, ex = ex)
    )
}

# disability free life expecancy with uncertainty measures

dfle_fun <- function(Lx, lx, Dx, qx, dr, var, ex, age){
    n <- length(age)
    Tx_i <- ex_i <- rep(0,n)

    Lx_i = Lx * (1 - dr)

    for(i in 1:n){
        Tx_i[i] <- sum(Lx_i[i:n])
        Tx_i <- replace(Tx_i,Tx_i[]==0,0.00000001)
        ex_i[i] <- Tx_i[i] / lx[i]
        ex_i[is.na(ex_i)] <- 0
    }
    Tx_i[n] <- Lx_i[n]
    ex_i[n] <- Tx_i[n]/lx[n]
    # variance in DFLE due to mortality
    var_mort_1 = (1/Dx)*qx^2*(1-qx)
    var_mort_2 = ((1-0.5)*5*(1-dr))+ex_i
    var_mort_tot = var_mort_2^2*lx*lx*var_mort_1
    # total variance
    var_tot_i = var + var_mort_tot

    return(dfle = list(age=age, ex=ex, dfle=ex_i, Lx_i=Lx_i, disrate=dr, variance=var_tot_i))
}

# Decomposition of HLE using a modified Arriaga decompositio (Nusselder and Looman 2004)
AdjustedArriaga <- function(Lx_y_1, Lx_y_2, frac_dis_x_y_1,frac_dis_x_y_2,Age){
    # adjustment for Nusselder & Looman formular (where l_0 = 1)
    Lx_y_1 <- Lx_y_1
    Lx_y_2 <- Lx_y_2
    MOR <- (((frac_dis_x_y_1)+(frac_dis_x_y_2))/2)*(Lx_y_2-Lx_y_1)
    # Change in the number  of  person  years  with  disability that  would  occur  if  only  mortality  rates  would  change
    DIS <- ((Lx_y_1+Lx_y_2)/2)*((1-frac_dis_x_y_2)-(1-frac_dis_x_y_1))
    return(cbind(Age,MOR,DIS))
}

# Calculation of changes in life without disability
AdjustedArriaga_dfle <- function(Lx_y_1, Lx_y_2, frac_dis_x_y_1,frac_dis_x_y_2,Age){
    # adjustment for Nusselder & Looman formular (where l_0 = 1)
    Lx_y_1 <- Lx_y_1
    Lx_y_2 <- Lx_y_2
    MOR <- (((1-frac_dis_x_y_1)+(1-frac_dis_x_y_2))/2)*(Lx_y_2-Lx_y_1)
    # Change in the number  of  person  years  with  disabilitythat  would  occur  if  only  mortality  rates  would  change
    DIS <- (Lx_y_1+Lx_y_2)/2*((1-frac_dis_x_y_2)-(1-frac_dis_x_y_1))
    return(cbind(Age,MOR,DIS))
}

# Estimation of DFLE due to mortality and disability based on decomposition contributions
dfle_fun_decomp <- function(Lx_dis_a, Lx_dis_b , lx_a, lx_b, MOR, DIS, ex, ex_2, age){
    n <- length(age)
    Tx_a <- ex_a <- rep(0,n)
    Tx_b <- ex_b <- rep(0,n)
    Tx_i <- ex_i <- rep(0,n)
    Tx_j <- ex_j <- rep(0,n)

    Lx_i = Lx_dis_a + MOR
    Lx_j = Lx_dis_a + DIS

    for(i in 1:n){
        # year 1
        Tx_a[i] <- sum(Lx_dis_a[i:n])
        Tx_a <- replace(Tx_a,Tx_a[]==0,0.00000001)
        ex_a[i] <- Tx_a[i] / lx_a[i]
        ex_a[is.na(ex_a)] <- 0
        # year 2
        Tx_b[i] <- sum(Lx_dis_b[i:n])
        Tx_b <- replace(Tx_b,Tx_b[]==0,0.00000001)
        ex_b[i] <- Tx_b[i] / lx_b[i]
        ex_b[is.na(ex_b)] <- 0
        # mortality
        Tx_i[i] <- sum(Lx_i[i:n])
        Tx_i <- replace(Tx_i,Tx_i[]==0,0.00000001)
        ex_i[i] <- Tx_i[i] / lx_a[i]
        ex_i[is.na(ex_i)] <- 0
        # disability
        Tx_j[i] <- sum(Lx_j[i:n])
        Tx_j <- replace(Tx_j,Tx_j[]==0,0.00000001)
        ex_j[i] <- Tx_j[i] / lx_a[i]
        ex_j[is.na(ex_j)] <- 0
    }
    Tx_a[n] <- Lx_dis_a[n]
    ex_a[n] <- Tx_a[n]/lx_a[n]
    Tx_b[n] <- Lx_dis_b[n]
    ex_b[n] <- Tx_b[n]/lx_b[n]
    Tx_i[n] <- Lx_i[n]
    ex_i[n] <- Tx_i[n]/lx_a[n]
    Tx_j[n] <- Lx_j[n]
    ex_j[n] <- Tx_j[n]/lx_a[n]

    return(dfle_decomp = list(age=age, ex=ex, ex_2=ex_2, dfle_1=ex_a, dfle_2=ex_b, dfle_mor=ex_i,
                              dfle_dis=ex_j, Lx_i=Lx_i, Lx_j=Lx_j))
}

####
stratified <- function(df, group, size, select = NULL,
                       replace = FALSE, bothSets = FALSE) {
    if (is.null(select)) {
        df <- df
    } else {
        if (is.null(names(select))) stop("'select' must be a named list")
        if (!all(names(select) %in% names(df)))
            stop("Please verify your 'select' argument")
        temp <- sapply(names(select),
                       function(x) df[[x]] %in% select[[x]])
        df <- df[rowSums(temp) == length(select), ]
    }
    df.interaction <- interaction(df[group], drop = TRUE)
    df.table <- table(df.interaction)
    df.split <- split(df, df.interaction)
    if (length(size) > 1) {
        if (length(size) != length(df.split))
            stop("Number of groups is ", length(df.split),
                 " but number of sizes supplied is ", length(size))
        if (is.null(names(size))) {
            n <- setNames(size, names(df.split))
            message(sQuote("size"), " vector entered as:\n\nsize = structure(c(",
                    paste(n, collapse = ", "), "),\n.Names = c(",
                    paste(shQuote(names(n)), collapse = ", "), ")) \n\n")
        } else {
            ifelse(all(names(size) %in% names(df.split)),
                   n <- size[names(df.split)],
                   stop("Named vector supplied with names ",
                        paste(names(size), collapse = ", "),
                        "\n but the names for the group levels are ",
                        paste(names(df.split), collapse = ", ")))
        }
    } else if (size < 1) {
        n <- round(df.table * size, digits = 0)
    } else if (size >= 1) {
        if (all(df.table >= size) || isTRUE(replace)) {
            n <- setNames(rep(size, length.out = length(df.split)),
                          names(df.split))
        } else {
            message(
                "Some groups\n---",
                paste(names(df.table[df.table < size]), collapse = ", "),
                "---\ncontain fewer observations",
                " than desired number of samples.\n",
                "All observations have been returned from those groups.")
            n <- c(sapply(df.table[df.table >= size], function(x) x = size),
                   df.table[df.table < size])
        }
    }
    temp <- lapply(
        names(df.split),
        function(x) df.split[[x]][sample(df.table[x],
                                         n[x], replace = replace), ])
    set1 <- do.call("rbind", temp)

    if (isTRUE(bothSets)) {
        set2 <- df[!rownames(df) %in% rownames(set1), ]
        list(SET1 = set1, SET2 = set2)
    } else {
        set1
    }
}

Dfle_Fun_Ohne <- function(Lx, lx, dr, age){
    n <- length(age)
    Tx_i <- ex_i <- rep(0,n)

    Lx_i = Lx * (1 - dr)

    for(i in 1:n){
        Tx_i[i] <- sum(Lx_i[i:n])
        Tx_i <- replace(Tx_i,Tx_i[]==0,0.00000001)
        ex_i[i] <- Tx_i[i] / lx[i]
        ex_i[is.na(ex_i)] <- 0
    }
    Tx_i[n] <- Lx_i[n]
    ex_i[n] <- Tx_i[n]/lx[n]

    return(dfle = list(age=age, dfle=ex_i, Lx_i=Lx_i, disrate=dr))
}


# age to age group
cutAges = function(variable,
                   agebreaks = c(65,70,75,80,85,90,105),
                   agelabels = c("65-69","70-74","75-79","80-84","85-89","90 plus")) {
  cut(variable, breaks = agebreaks, labels = agelabels, right = FALSE)
}
