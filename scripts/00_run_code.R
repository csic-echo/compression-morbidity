######################
# run code
######################


# define options
knit_by_pkgdown <- !is.null(knitr::opts_chunk$get("fig.retina"))
pander::panderOptions("table.split.table", Inf)
ggplot2::theme_set(ggplot2::theme_bw())
knitr::opts_chunk$set(warning = TRUE, message = TRUE,
                      error = FALSE, echo = TRUE)
# source files in order
source("scripts/01_transform_data.R")
source("scripts/02_preparing chronic disease rates.R")
source("scripts/03_analysis.R")
source("scripts/04_decomposition_dfle.R")
source("scripts/04b_decomposition_chrdfle.R")
# Warning! The bootstrap will take up to 5 min
source("scripts/05_bootstrap.R")