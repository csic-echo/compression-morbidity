## Estimating Education Trends in Healthy Life Expectancy in Spain 2008-2017

## What is this project about?
- The aim of this project was to confirm if there was a "new" expansion of morbidity in Spain as recently published research indicated. This descriptive finding was then expanded in three ways. First, we use different health indicators to confirm or disprove combinded trends of morbidity, mortality and disability. Second, we describe the impact of education differences over time. Third and last, we are decomposing the changes to assess the contribution of disability, chronic illness, and mortality to our healthy life expectancy measures.

## Data availability
- A minimal sample of anonymized, individual level information, methodology and all questionnaires of the Spanish national survey on disability, autonomy, personal situations and dependency (“Encuesta sobre discapacidades, autonom´ıa personal y situaciones de dependencia”; short: EDAD) and the Spanish National Health Survey 2017 (”Encuesta Nacional de Salud”) can be downloaded from the website of the Spanish National Institute of Statistics (INE) without further costs. (Link: EDAD: https://bit.ly/32EO9Oh and ENSE: https://bit.ly/3gL6deG)
This data was used to extract the age-specific disability rates for the two periods. The mortality data that we used to estimate the HLE measures is unfortunately not available publicly (see data agreement BE014/2018).

- This data can be requested through the website of the National Institute for Statistics (INE; Link: https://www.ine.es).

## How to use the code
- Although the mortality information is not freely available by education, the two Sullivan life tables, uncertainty measures and decomposition can still be obtained when an alternative data source for the death counts and exposure is used.

- After the data has been downloaded, the whole analysis can be executed by running "00_run_code.R". Single parts of the process, such as the analysis or the bootstrap, can be executed by running the respective files.

  